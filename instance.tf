provider "aws" {
  access_key = var.access_key_id
  secret_key = var.secret_key_id
  region = var.AWS_REGION
}

variable "AWS_REGION" {
  type = string
}

variable "access_key_id" {
  type = string
}

variable "secret_key_id" {
  type = string
}

variable "ansible_engine_eip_allct_id" {
  type = string
}

resource "aws_security_group" "ansible-engine-instance-sg" {
  name = "ansible-engine-instance-sg"
  description = "Security Group for terraform instances."
  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }


  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "ansible-engine-instance-sg"
  }


}

resource "aws_instance" "ansible-engine" {
  ami = "ami-014d4cac856efc4eb"
  instance_type = "t2.micro"  
  key_name = "Ansible-Tower-Key"

  tags = {
    Name = "ansible-engine"
  }

  vpc_security_group_ids = [ aws_security_group.ansible-engine-instance-sg.id ]

  user_data = <<-EOF
    #!/bin/bash
    
    cd /home/ansadmin
    git clone https://github.com/arjunbnair97/trojanwall-ansible-engine.git ansible-workdir

    EOF
  
}


resource "aws_eip_association" "ansible-engine-eip-assn" {
  instance_id   = aws_instance.ansible-engine.id
  allocation_id = var.ansible_engine_eip_allct_id
}

